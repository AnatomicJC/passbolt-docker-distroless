#!/usr/bin/env bash
set -euo pipefail

docker-compose down -t 1

docker-compose -f docker-compose.passbolt-update.yml up -d

cat << EOF | docker-compose -f docker-compose.passbolt-update.yml run --rm -T --entrypoint ash -u0 passbolt-admin
rm -rf *
rsync -a /opt/passbolt/ /var/www/passbolt/
EOF

cat << EOF | docker-compose -f docker-compose.passbolt-update.yml run --rm -T --entrypoint ash passbolt-admin
/usr/bin/env bash /usr/local/bin/wait_for.sh -t 0 db:3306 -- /bin/bash /var/www/passbolt/bin/cake passbolt migrate
EOF

cat << EOF | docker-compose -f docker-compose.passbolt-admin.yml run --rm -T --entrypoint ash -u0 passbolt-admin
chown -R 100:101 /var/lib/mysql
rsync -a /opt/passbolt/ .
chown -R www-data: /var/www/passbolt
EOF

docker-compose up -d
