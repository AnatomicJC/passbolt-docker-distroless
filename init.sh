#!/usr/bin/env bash
set -euo pipefail

get_kernel () {
  uname | tr '[:upper:]' '[:lower:]'
}

SED_BINARY="sed"

if [ "$(get_kernel)" = "darwin" ]
then
  SED_BINARY="gsed"
fi

if [ ! -f .env ]
then
  cp .env.sample .env
fi

if [ ! -f docker-compose.yml ]
then
  cp docker-compose.yml.sample docker-compose.yml
fi

docker-compose -f docker-compose.init.yml up --force-recreate -d
sleep 10
docker-compose -f docker-compose.init.yml down --remove-orphans

cat << EOF | docker-compose -f docker-compose.passbolt-admin.yml run --rm -T --entrypoint /bin/ash -u0 passbolt-admin
chown -R 100:101 /var/lib/mysql
rsync -a /opt/passbolt/ .
chown -R www-data: /var/www/passbolt
EOF

docker-compose up -d

if [ -f subscription_key.txt ]
then
  docker-compose run --rm -T --entrypoint tee passbolt-admin -a /var/www/passbolt/config/subscription_key.txt < subscription_key.txt > /dev/null
fi

docker-compose --env-file .env --rm run -T --entrypoint ash passbolt-admin < first-setup.sh | tee output

FINGERPRINT=$(tail -n1 output)
${SED_BINARY} -i "s/\(^PASSBOLT_GPG_SERVER_KEY_FINGERPRINT=\"\)\(.*\)\(\"$\)/\1${FINGERPRINT}\3/g" .env
rm output

docker-compose down --remove-orphans -t 1

docker-compose up --remove-orphans -d
