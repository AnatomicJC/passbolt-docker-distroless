#!/usr/bin/env bash

mkdir -m 0700 gpg-tmp

gpg --homedir gpg-tmp --batch --no-tty --gen-key <<EOF > /dev/null 2>&1
    Key-Type: eddsa
    Key-Curve: ed25519
    Key-Usage: sign,cert
    Subkey-Type: ecdh
    Subkey-Curve: cv25519
    SubKey-Usage: encrypt
    Name-Real: ${PASSBOLT_INIT_GPG_USERNAME}
    Name-Email: ${PASSBOLT_INIT_GPG_EMAIL}
    Expire-Date: 0
    %no-protection
    %commit
EOF

FINGERPRINT="$(gpg --homedir gpg-tmp --list-keys | grep -Ev "(pub|uid|sub|---|^$)" | sed 's/ //g')"

gpg --armor --homedir gpg-tmp --export "${FINGERPRINT}" > /var/www/gpg/serverkey.gpg
gpg --armor --homedir gpg-tmp --export-secret-keys "${FINGERPRINT}" > /var/www/gpg/serverkey_private.gpg

rm -rf gpg-tmp

gpg --home /home/www-data/.gnupg --import /var/www/gpg/serverkey_private.gpg
/usr/bin/env bash /var/www/passbolt/bin/cake passbolt install --no-admin
/usr/bin/env bash /var/www/passbolt/bin/cake passbolt migrate
/usr/bin/env bash /var/www/passbolt/bin/cake passbolt register_user -u "${PASSBOLT_INIT_ADMIN_EMAIL}" -f "${PASSBOLT_INIT_ADMIN_NAME}" -l "${PASSBOLT_INIT_ADMIN_SURNAME}" -r admin
echo "${FINGERPRINT}"
