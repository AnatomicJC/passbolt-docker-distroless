Put passbolt-email.service and passbolt-email.timer on /etc/systemd/system

Reload systemd:

```bash
systemctl daemon-reload
```

Enable services:

```
systemctl enable passbolt-email.service
systemctl enable passbolt-email.timer
```

To display logs:

```
journalctl -f -u passbolt-email
```
