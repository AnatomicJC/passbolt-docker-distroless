#!/usr/bin/env bash
set -euo pipefail

display_help() {
cat << EOF
  Usage:
    ./passbolt-tarball.sh flavour version
      Will fetch passbolt source code and create a tarball
      eg. ./passbolt-tarball.sh pro 3.2.2
EOF
exit 0
}
if [ "$#" -lt 2 ]
then
    display_help
else
    FLAVOUR="${1}"
    PKG_VERSION="${2}"
fi

tarball_create () {
    local FLAVOUR="${1}"
    local PKG_VERSION="${2}"
    # Clean
    rm -rf passbolt
    # Clone Passbolt release
    if [ "${FLAVOUR}" = "pro" ]
    then
      local PASSBOLT_REPO="https://bitbucket.org/passbolt_pro/passbolt_${FLAVOUR}_api"
    else
      local PASSBOLT_REPO="https://github.com/passbolt/passbolt_api"
    fi
    git clone --depth=1 --branch=v"${PKG_VERSION}" "${PASSBOLT_REPO}" passbolt
    # Install vendors
    cd passbolt
    docker run -ti --rm -v "${PWD}:/app" composer:2.1.8 install --no-dev -o --prefer-dist --ignore-platform-reqs --no-interaction
    # Apply security fixes
    sed -i '/"name": "laminas\/laminas-diactoros",/{n;s/.*/"version": "2.25.2",/}' composer.lock
    # remove gpg unsecure keys
    rm -f config/gpg/unsecure_private.key
    rm -f config/gpg/unsecure.key
    cd -
    #sudo chown -R 82:82 passbolt
    # Make a tarball
    tar --exclude .git --create --file passbolt.tar.gz passbolt

    rm -rf passbolt
}

tarball_create "${FLAVOUR}" "${PKG_VERSION}"
