#!/usr/bin/env bash
set -euo pipefail

cat << EOF | docker-compose run --rm -T --entrypoint ash passbolt-admin
/usr/bin/env bash ./bin/cake passbolt subscription_import
EOF
