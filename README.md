# passbolt-docker-distroless

[[_TOC_]]

This repository hosts needed stuff to build a distroless passbolt instance.

* [Dockerfiles](./dockerfiles)
* [CI/CD](../../pipelines)
* Example scripts

## Why distroless ?

In a distroless docker image, you won’t find any shell, package manager or utilities such as grep, sed, awk, … It runs only your application and nothing else.

Don’t expect to launch commands like `docker run -it my-distroless-image something`, it will fail with a message like this one:

```
standard_init_linux.go:228: exec user process caused: no such file or directory
```

It is better for security.

You can read more on these 2 blog posts:

* [passbolt blog post about distroless containers](https://blog.passbolt.com/improving-passbolt-security-with-distroless-containers-3ed58e5791de)
* [how to build an alpine distroless image](https://jcvassort.open-web.fr/how-to-build-alpine-distroless-docker-images/)

## How does it works ?

Basically, you will need to run 4 distroless containers to make passbolt work:

* mariadb for database
* nginx as web server
* php for passbolt code
* redis to handle php sessions

I personnaly use this stack on my own self-hosted passbolt instance, plus a traefik container to handle SSL termination.

To perform passbolt admin tasks, such as debugging or update, a `passbolt-admin` container is available.

To handle passbolt email sending, a shell access is however required. You will find in the docker-compose.yml example file a passbolt-admin service to handle mail sending. On my personal server, I created a systemd service ([click here to see the service files](./systemd))

## Show me how to spin my instance

You can spin a demo with docker-compose files from this repo. As passbolt setup in a distroless environment is not an easy task, I wrote shell scripts for that.

### Prerequisites

Before initializing your passbolt instance, you will have to customize your setup. This is optional if you just want to spin a demo.

Copy `.env.sample` to `.env` and set your environment variables.

Copy `.docker-compose.yml.sample` to `.docker-compose.yml`. You can remove the mail service if you plan to use a real SMTP server and not use this stack for demo purposes.

If you are a pro user, replace `ce` with `pro` in .env file and create a `subscription_key.txt` file containing you subscription key. Your key will be inserted during the creation process.

### Init

Launch the `init.sh` script to create your instance, the passbolt install log will be displayed with a register link at the end of the process like this one:

```
     ____                  __          ____
    / __ \____  _____ ____/ /_  ____  / / /_
   / /_/ / __ `/ ___/ ___/ __ \/ __ \/ / __/
  / ____/ /_/ (__  |__  ) /_/ / /_/ / / /
 /_/    \__,_/____/____/_.___/\____/_/\__/

 Open source password manager for teams
-------------------------------------------------------------------------------
User saved successfully.
To start registration follow the link provided in your mailbox or here:
http://localhost:8080/setup/install/da7d6a98-c4a8-4620-917a-4c56a0f8ef04/9df8f3ec-70cc-4dd6-93b3-0e70315a2549
```

### Register your first admin user

Open a browser, use follow the link from previous step to create your first admin user.

### Import your subscription key (optional and only for PRO users)

This step will import your subscription key in the database

```
sh subscription_import.sh
```

### Tools

Some tools are included in passbolt-admin container, and located in `/usr/local/bin` directory.

* `passbolt-mysql` will connect directly to passbolt database in a MySQL console.
* `passbolt-mysqldump` will dump your database in standard output
* `passbolt-import-database` will drop your current database to restore your database dump

You have also the passbolt `./bin/cake` command line. Launch it with `--help` argument to get available parameters.

For each parameter, you can use the `--help` flag too.

### Import a previous passbolt database instance

I previously had passbolt installed on another server. I made a compressed backup of my database and named it passbolt.sql.gz.

```
mysqldump passbolt | gzip > passbolt.sql.gz
```

From docker-compose.yml, in  passbolt-admin service, mount your dump in /passbolt.sql.gz by adding a new line in volumes definition:

```
  passbolt-admin:
    image: anatomicjc/passbolt-admin:${PASSBOLT_VERSION}
    (...)
    volumes:
      - passbolt_volume:/var/www/passbolt/
      - passbolt_gpg_home:/home/www-data/.gnupg
      - passbolt_gpg_keys:/var/www/gpg
      - passbolt_tmpfs:/run/php
      - ./passbolt.sql.gz:/passbolt.sql.gz
```

Launch a passbolt-admin container instance:

```
docker-compose run --entrypoint ash passbolt-admin
```

And import your database with the dedicated `passbolt-import-database` tool:

```
passbolt-import-database
```

You can also add as first argument the path to your database dump file.

Once import finished, launch passbolt database migrations and clear the cache:

```
./bin/cake migrations migrate
./bin/cake cache clear_all
```

## Taskfile

This repository contains a Taskfile (a Makefile alternative) to make your life easier.

You can read this blog post if you don't know what it is: https://dev.to/stack-labs/introduction-to-taskfile-a-makefile-alternative-h92

Possible actions:

```
$ task --list
task: Available tasks for this project:
* default:              the default task
* destroy:              Will destroy passbolt containers and volumes
* init:                 Initialize a new distroless passbolt instance
* logs:                 Get docker logs `task logs -- --tail=10 web` (can be db, php, web, passbolt-admin, redis, mail)
* passbolt-admin:       Instanciate a passbolt-admin container to let you perform admin tasks
* passbolt-admin-root:  Instanciate a passbolt-admin container to let you perform admin tasks
* relaunch-containers:  Will recreate docker containers
* reset-repo:           Will remove .env and docker-compose.yml files
* update:               Will update your passbolt instance
```

task logs example usage:

`task logs` will display all logs
`task logs -- web` will display only logs from web service (possible values: db, php, web, passbolt-admin, redis, mail)

Please enjoy!
